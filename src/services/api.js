import axios from 'axios';

const API = 'http://localhost:8080';
const supplierId = localStorage.getItem('id');
const headers = {
 'Content-Type': 'application/json'
}

export default function fetchCustomers() {
  return axios.get(API + '/supplier/v1/supplier/'+supplierId+'/shops/search', { headers: headers, withCredentials: true });
}
