//ModalComponent.js
import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import axios from 'axios';
import Moment from 'react-moment';
import './edit-customer-modal.css';
import AppConstants from '../../constants/appconstants';
import i18n from '../../i18n';
import { withTranslation  } from 'react-i18next';

class EditCustomerModal extends React.Component {
    customerName='';
    canEdit = true;
    countryNames = AppConstants.countryList;
    baseUrl= 'http://localhost:8080';
    vatregimesArray = JSON.parse(JSON.stringify(AppConstants.vatregimesArray));
    validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
    validTelephoneRegex = RegExp(/([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/);
    constructor(props) {
        super(props);
        this.state = {formValid: false,
            errorCount: null,
            errors: {
              name: '',
              email: '',
              telephone: '',
              btw: '',
              contactPerson: ''
            },
            isArchived: true
        };
    }

    componentDidMount() {
        const customerId = this.props.data;
        const supplierId = localStorage.getItem('id');
        const headers = {
            'Content-Type': 'application/json'
        }
        axios.get(this.baseUrl + '/supplier/v1/supplier/' + supplierId + '/shop/' + customerId, 
        { withCredentials: true, headers: headers }).then(response => {
          const record = response['data'];
          this.customerName = record['name'];
          this.setState({
              name: record['name'],
              created: record['createdAt'],
              updated: record['updatedAt'],
              dropShippingStatus: record['dropShipEnabled'] != null ? (record['dropShipEnabled'] == 1 || record['dropShipEnabled'] == true ? true : false) : false,
              checkboxMinOrderAmountStatus: record['dropShipWithoutMinimumEnabled'] != null ? (record['dropShipWithoutMinimumEnabled'] == 1 || record['dropShipWithoutMinimumEnabled'] == true ? true : false) : false,
              perfoma: record['perfomaIndicator'] != null ? (record['perfomaIndicator'] == 1 || record['perfomaIndicator'] == true ? true: false): false,
              id: record['id'],
              contactPerson: record['contactPerson'],
              name: record['name'],
              email: record['email'],
              telephone: record['contactNumber'],
              street: record['deliveryAddress'] != null ? record['deliveryAddress'].streetName : '',
              nr: record['deliveryAddress'] != null ? record['deliveryAddress'].streetNumber : '',
              bus: record['deliveryAddress'] != null ? record['deliveryAddress'].postBox : '',
              addressLine: record['deliveryAddress'] != null ? record['deliveryAddress'].addressLine : '',
              postCode: record['deliveryAddress'] != null ? record['deliveryAddress'].zip : '',
              city: record['deliveryAddress'] != null ? record['deliveryAddress'].city : '',
              country: record['deliveryAddress'] != null ? record['deliveryAddress'].country : '',
              invoice_street: record['invoiceAddress'] != null ? record['invoiceAddress'].streetName : '',
              invoice_nr: record['invoiceAddress'] != null ? record['invoiceAddress'].streetNumber : '',
              invoice_bus: record['invoiceAddress'] != null ? record['invoiceAddress'].postBox : '',
              invoice_addressLine: record['invoiceAddress'] != null ? record['invoiceAddress'].addressLine : '',
              invoice_postCode: record['invoiceAddress'] != null ? record['invoiceAddress'].zip : '',
              invoice_country: record['invoiceAddress'] != null ? record['invoiceAddress'].country : '',
              btw: record['vat'] != null ? record['vat'] : '',
              vatRegime: record['vatRegime'] != null ? record['vatRegime'] : '',
              categoryName: record['categoryId'] ? record['categoryId'] : null,
              internnumber: record['internNumber'] != null ? record['internNumber'] : null,
              francoorder: +record['francoOrder'] != null ? record['francoOrder'] : null,
              invoice_city: record['invoiceAddress'] != null ? record['invoiceAddress'].city : '',
              minimumorder: +record['minimumOrder'] != null ? record['minimumOrder'] : null,
              avgdeliverydays: +record['avgDeliveryDays'] != null ? record['avgDeliveryDays'] : null,
              dropShipMinimumOrder: +record['dropShipOrder'] != null ? record['dropShipOrder'] : null,
              transpotationCost: +record['transportationCost'] != null ? record['transportationCost'] : null,
              transpotationCostWithoutMin: +record['transportationCostWithoutMinimumOrder'] != null ? record['transportationCostWithoutMinimumOrder'] : null
          })
        });
    }

    saveData() {
        if(this.validateForm(this.state.errors)) {
        const customerId = this.props.data;
        const supplierId = localStorage.getItem('id');
        const headers = {
            'Content-Type': 'application/json'
        }
        let dataToSubmit = {
            "email": this.state.email,
            "name": this.state.name,
            "reuseVat": false,
            "vat": this.state.btw,
            "vatRegime": this.state.vatRegime,
            "country": this.state.country,
            "contactNumber": this.state.telephone,
            "contactPerson": this.state.contactPerson,
            "deliveryAddress": {
              "streetName": this.state.street,
              "streetNumber": this.state.nr,
              "postBox": this.state.bus,
              "zip": this.state.postCode,
              "city": this.state.city,
              "addressLine": this.state.addressLine,
              "country": this.state.country
            },
            "invoiceAddress": {
              "streetName": this.state.invoice_street,
              "streetNumber": this.state.invoice_nr,
              "postBox": this.state.invoice_bus,
              "zip": this.state.invoice_postCode,
              "city": this.state.invoice_city,
              "addressLine": this.state.invoice_addressLine,
              "country": this.state.invoice_country,
            },
            "country": this.state.country,
            "contactNumber": this.state.telephone,
            "internNumber": this.state.internnumber,
            "francoOrder": this.state.francoorder,
            "minimumOrder": this.state.minimumorder,
            "avgDeliveryDays": this.state.avgdeliverydays,
            "perfomaIndicator": this.state.perfoma == true ? 1 : 0,
            "dropShipMinimumOrder": this.state.dropShipMinimumOrder == true ? 1: 0,
            "dropShipping": this.state.dropShippingStatus == true ? 1 : 0,
  
            "transportationCost": this.state.transpotationCost,
            "dropshipWithoutMinimum": this.state.checkboxMinOrderAmountStatus == true ? 1 : 0,
            "transportationCostWithoutMinimum": this.state.transpotationCostWithoutMin,
            "contactPerson": this.state.contactPerson
          }  
          axios.put(this.baseUrl + '/supplier/v1/supplier/' + supplierId + '/shop/' + customerId,
          dataToSubmit, { withCredentials: true, headers: headers }).then(res => {
              if(res.data.status) {
                alert('customer data saved successfully');
                this.props.close();
                this.props.onLoad();
              }
          });
        }
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    validateForm = (errors) => {
        let valid = true;
        Object.values(errors).forEach(
          (val) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({
            [name]: value
        }
        )
        let errors = this.state.errors;
        switch (name) {
            case 'name': 
              errors.name = 
                value.trim().length == 0
                  ? 'Name is required'
                  : '';
              break;
            case 'email': 
              errors.email = 
                this.validEmailRegex.test(value)
                  ? ''
                  : 'Email is not valid!';
              break;
            case 'telephone':
                errors.telephone =
                this.validTelephoneRegex.test(value)
                ? ''
                : 'Please enter a valid telephone number';
            break;
            case 'contactPerson':
              errors.contactPerson = 
                value.trim().length == 0
                ? 'ContactPerson is required'
                : '';  
            default:
              break;
          }
    }

    render() {
        const { t } = this.props;
        const { errors } = this.state
        return (
            <div>
                <Modal isOpen={this.props.showModal}>
                    <ModalHeader style={{backgroundColor: '#fafbfc', height: '80px'}}>
                    <div style={{float: 'left'}}>
                    <div _ngcontent-jqj-c14="" className="wrapword">{ this.customerName }</div>
                    <label _ngcontent-jqj-c14="" className="small-text" style={{textAlign: 'left'}}>Customer</label>
                    </div>
                    <div _ngcontent-jqj-c14="" className="actions pull-right">
                            <button _ngcontent-jqj-c14="" className="btn btn-action btn-primary bluebg mt-10" onClick={ () =>  this.saveData() }> Save </button>
                            <button _ngcontent-jqj-c14="" onClick={this.props.close} className="btn btn-action btn-primary bluebg mt-10"> Cancel </button></div>
                    </ModalHeader>
                    <ModalBody className="modal-body">
                        <div className="content-wrapper">
                            <form id="ember2737" className="edit-page ember-view" onSubmit={e => this.handleSubmit(e)}>
                                <div className="row p-0 col-lg-12 col-md-12 col-sm-12 mx-auto" style={{ marginRight: '0px !important' }}>
                                    <div className="col-lg-4 col-md-5 col-sm-5">
                                        <div className="section-default  grey-border">
                                            <div className="panel-body">
                                                <table className="info">
                                                    <tbody>
                                                        <tr>
                                                            <td className="name"><label className="col-form-label fontbold">
                                                                Status</label></td>
                                                            <td>
                                                                <span className="label label-success">
                                                                    <button
                                                                        className={'active-status-btn' + (!this.state.isArchived ? ' inactive-button' : '')}>
                                                                        {this.state.isArchived ? 'Active' : 'Inactive'}
                                                                    </button>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="name"> <label className="col-form-label fontbold">
                                                                Created</label></td>
                                                            <td>
                                                                <div className="col-sm-8 pr-0" className="form-control-readOnly">
                                                                <Moment format="DD/MM/YYYY" unix>{this.state.created}</Moment>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="name"> <label className="col-form-label fontbold">
                                                                Updated</label></td>
                                                            <td>
                                                                <div className="col-sm-8 pr-0" className="form-control-readOnly">
                                                                    <Moment format="DD/MM/YYYY" unix>{this.state.updated}</Moment>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <br />
                                        </div>

                                        <div>
                                            <br />
                                        </div>
                                        <div className="section-default  grey-border">
                                            <div className="section-heading">Contact</div><br />
                                            <div className="panel-body">
                                                <div className="form-horizontal" role="form">
                                                    <div className="form-group row mx-auto">
                                                        <label htmlFor="inputPassword" className="col-sm-2 col-form-label fontbold"
                                                            style={{ whiteSpace: 'nowrap' }}><em className="fa fa-at"></em>
                                                            &nbsp;<i className="required">*</i></label>
                                                        <div className="col-sm-10 pr-0">

                                                            <input disabled={this.canEdit == true ? true : null} type="email"
                                                                className={"form-control" + (this.canEdit ? " disabled-form-control" : '')}
                                                                placeholder={t('UserDetails.email')} name="email"
                                                                maxLength="120" defaultValue={ this.state.email }/>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group row mx-auto">
                                                    <label htmlFor="inputPassword" className="col-sm-2 col-form-label fontbold"><em
                                                        className="fa fa-phone"></em></label>
                                                    <div className="col-sm-10 pr-0">
                                                        <input type="tel" maxLength="15" className="form-control" name="telephone"
                                                             placeholder={t('UserDetails.telephone')} value={ this.state.telephone } onChange={this.handleChange}/>
                                                            {errors.telephone.length > 0 ? <div>
                                                                <label className="errorMessage" style={{textAlign: 'left'}}>{errors.telephone}</label>
                                                            </div>: ''}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div>
                                                    <div id="ember2742" className="ember-view">
                                                        <div className="address-edit-field">
                                                            <table style={{ width: '100%' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <th className="section-heading">
                                                                            <label style={{ whiteSpace: 'nowrap' }}
                                                                                className="col-md-3 control-label text-center fontbold"><em
                                                                                    className="fa fa-envelope"></em>
                                                                &nbsp;
                                                                Delivery Address</label>
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <input type="text"
                                                                                className="form-control ember-view ember-text-field"
                                                                                maxLength="100" name="street"
                                                                                placeholder={t('UserDetails.street')} value={ this.state.street } onChange={this.handleChange}/>
                                                                        </td>
                                                                        <td style={{ width: '60px', paddingLeft: '2px' }}><input
                                                                            type="text" maxLength="40" name="nr"
                                                                            placeholder={t('UserDetails.Nr')}
                                                                            className="form-control ember-view ember-text-field" value={ this.state.nr} onChange={this.handleChange}/>
                                                                        </td>
                                                                        <td style={{ width: '60px', paddingLeft: '2px' }}><input
                                                                            type="text" maxLength="40" name="bus"
                                                                            placeholder={t('UserDetails.bus')}
                                                                            className="form-control ember-view ember-text-field" value={ this.state.bus} onChange={this.handleChange}/>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style={{ width: '100%', marginTop: '1px' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><input type="text" name="addressLine"
                                                                            maxLength="100" onChange={e => this.setState({addressLine: e.target.value})}
                                                                            placeholder={t('UserDetails.addressLine')} value={ this.state.addressLine }
                                                                            className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style={{ width: '100%', marginTop: '1px' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style={{ width: '50%', paddingRight: '1px' }}><input name="postCode"
                                                                            type="text" maxLength="25" onChange={e => this.setState({postCode: e.target.value})}
                                                                            placeholder={t('UserDetails.postcode')} value={ this.state.postCode }
                                                                            className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                        <td style={{ width: '50%', paddingLeft: '1px' }}><input name="city"
                                                                            id="ember2748" type="text" maxLength="100" onChange={e => this.setState({city: e.target.value})}
                                                                            placeholder={t('UserDetails.city')} value={ this.state.city }
                                                                            className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style={{ width: '100%', marginTop: '1px' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <div id="ember2749" className="ember-view">
                                                                                <select className="form-control form-control" value={ this.state.country }
                                                                                onChange={e => this.setState({country: e.target.value})}>
                                                                                    <option value="">
                                                                                        --Select country--
                                                                            </option>
                                                                                    {this.countryNames.map((country, index) =>
                                                                                        <option key={index} value={country.code}>{country.name}</option>
                                                                                    )}

                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div id="ember2742" className="ember-view">
                                                        <div className="address-edit-field">
                                                            <table style={{ width: '100%' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <th className="section-heading">
                                                                            <label style={{ whiteSpace: 'nowrap' }}
                                                                                className="col-md-3 control-labe'l text-center fontbold"><em
                                                                                    className="fa fa-envelope"></em>
                                                                &nbsp;
                                                                Invoice Address</label>
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <input type="text"
                                                                                maxLength="100" onChange={e => this.setState({invoice_street: e.target.value})}
                                                                                placeholder="Street" value={ this.state.invoice_street }
                                                                                className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                        <td style={{ width: '60px', paddingLeft: '2px' }}><input
                                                                            type="text" maxLength="40" onChange={e => this.setState({invoice_nr: e.target.value})}
                                                                            placeholder="Nr" value={ this.state.invoice_nr }
                                                                            className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                        <td style={{ width: '60px', paddingLeft: '2px' }}><input
                                                                            type="text" maxLength="40" onChange={e => this.setState({invoice_bus: e.target.value})}
                                                                            placeholder="Bus" value={ this.state.invoice_bus }
                                                                            className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style={{ width: '100%', marginTop: '1px' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <td><input type="text" maxLength="100" onChange={e => this.setState({invoice_addressLine: e.target.value})}
                                                                            placeholder="Address Line" value={ this.state.invoice_addressLine }
                                                                            className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style={{ width: '100%', marginTop: '1px' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style={{ width: '50%', paddingRight: '1px' }}><input
                                                                            type="text"
                                                                            maxLength="20"
                                                                            placeholder="Post code" onChange={e => this.setState({invoice_postCode: e.target.value})}
                                                                            className="form-control ember-view ember-text-field" value={this.state.invoice_postCode}/>
                                                                        </td>
                                                                        <td style={{ width: '50%', paddingLeft: '1px' }}><input
                                                                            id="ember2748" type="text" maxLength="100" onChange={e => this.setState({invoice_city: e.target.value})}
                                                                            placeholder="City" value={this.state.invoice_city}
                                                                            className="form-control ember-view ember-text-field" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table style={{ width: '100%', marginTop: '1px' }}>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <div id="ember2749" className="ember-view">
                                                                                <select className="form-control form-control" onChange={e => this.setState({invoice_country: e.target.value})} value= { this.state.invoice_country }>
                                                                                    <option value="">
                                                                                        --Select country--
                                                                            </option>
                                                                                    {this.countryNames.map((country, index) =>
                                                                                        <option key={index} value={country.code}>{country.name}</option>
                                                                                    )}

                                                                                </select>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-lg-8 col-md-7 col-sm-7">


                                        <div className="section-default grey-border height33">
                                            <div className="section-heading">General</div><br />
                                            <div className="panel-body">
                                                <div className="form-horizontal" role="form">
                                                    <div className="form-group row">
                                                        <label htmlFor="inputPassword"
                                                            className="col-sm-3 col-form-label fontbold">Id&nbsp;<i
                                                                className="required">*</i></label>
                                                        <div className="col-sm-9 pr-35">
                                                            <input type="text" className="form-control disabled-form-control"
                                                                placeholder="new" value= { this.state.id } onChange={e => this.setState({id: e.target.value})}
                                                                readOnly />
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="inputPassword"
                                                            className="col-sm-3 col-form-label fontbold">Name&nbsp;<i
                                                                className="required">*</i></label>
                                                        <div className="col-sm-9 pr-35">
                                                            <input type="text" className="form-control" maxLength="119" name="name"
                                                                placeholder="Name" value={ this.state.name } onChange={ e => this.handleChange(e) }/>
                                    {errors.name.length > 0 ? <div style={{textAlign: 'left'}}>
                                    <label className="errorMessage">{errors.name}</label>
                                </div>: ''}
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="inputPassword"
                                                            className="col-sm-3 col-form-label fontbold">Contact Person&nbsp;<i
                                                                className="required">*</i></label>
                                                        <div className="col-sm-9 pr-35">
                                                            <input type="text" className="form-control" maxLength="119" onChange={this.handleChange}
                                                              name="contactPerson"  placeholder="Contact Person" value={ this.state.contactPerson }/>
                                                            {errors.contactPerson.length > 0 ? <div style={{ textAlign: 'left'}}>
                                                                 <label className="errorMessage">{errors.contactPerson}</label>
                                                            </div>: ''}
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="inputPassword"
                                                            className="col-sm-3 col-form-label fontbold">Vat&nbsp;<i
                                                                className="required">*</i></label>
                                                        <div className="col-sm-9 pr-35">
                                                            <input type="text" className="form-control" maxLength="12" value={ this.state.btw }
                                                            name="btw" onChange={ this.handleChange } placeholder="Vat" />
                                                            {errors.btw.length > 0 ? <div>
                                                                 <label className="errorMessage">{errors.btw}</label>
                                                            </div>: ''}
                                                        </div>
                                                    </div>

                                                    <div className="form-group row">
                                                        <label htmlFor="inputPassword" className="col-sm-3 col-form-label fontbold"
                                                            style={{whiteSpace: 'nowrap'}}>Vat Regime
                    <i className="required">*</i></label>
                                                        <div className="col-sm-9 pr-35">
                                                            <select className="form-control form-control" onChange={e => this.setState({vatRegime: e.target.value})} 
                                                            value= { this.state.vatRegime }>
                                                                <option value="">--Select--</option>
                                                                {this.vatregimesArray.map((vats, index) =>
                                                                    <option key={index} value={vats.key}>{vats.value}</option>
                                                                )}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <br />
                                    <div className="section-default grey-border" style={{ height: '67%' }}>
                                        <div className="section-heading">Delivery</div><br />
                                        <div className="panel-body">
                                            <div className="form-horizontal" role="form">
                                                <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Franco Order&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">
                                                        <input type="number" className="form-control" value={ this.state.francoorder }
                                                            placeholder="Franco Order" onChange={e => this.setState({francoorder: e.target.value})} 
                                                            min="0" />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Intern Number&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">
                                                        <input type="number" className="form-control" value={ this.state.internnumber }
                                                            placeholder="Intern Number" onChange={e => this.setState({internnumber: e.target.value})} 
                                                            min="0" />
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Minimum Order&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">
                                                        <input type="number" className="form-control" value={ this.state.minimumorder }
                                                            placeholder="Minimum order" onChange={e => this.setState({minimumorder: e.target.value})}
                                                            min="0" />
                                                    </div>
                                                </div>

                                                <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Avg Delivery Days&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">
                                                        <input type="number" className="form-control" value={ this.state.avgdeliverydays }
                                                            placeholder="Avg Delivery Days" onChange={e => this.setState({avgdeliverydays: e.target.value})}
                                                            min="0" />
                                                    </div>
                                                </div>

                                                <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Perfoma&nbsp;</label>
                                                    <div className="col-sm-9 pr-35" style={{textAlign:'left'}}>
                                                        <input type="checkbox" placeholder="Perfoma" checked={this.state.perfoma} 
                                                            onChange={e => this.setState({perfoma: e.target.checked})} id="performaIndicator" />
                                                    </div>
                                                </div>
                                                <div className="form-group row" style={{textAlign:'left'}}>
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Drop shipping order&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">
                                                        <input type="checkbox" id="dropShipping" onChange={e => this.setState({dropShippingStatus: e.target.checked})} 
                                                        checked={this.state.dropShippingStatus}/>
                                                    </div>
                                                </div>
                                                {this.state.dropShippingStatus ? <div><div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Minimum Order Amount&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">

                                                        <input type="text" className="form-control" maxLength="10" value={ this.state.minimumorder }
                                                            min="0" onChange={e => this.setState({minimumorder: e.target.value})}
                                                            inputMode="numeric" type="number"
                                                            placeholder="Minimum Order Amount" />
                                                    </div>
                                                </div>

                                                <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Transportation Cost&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">
                                                        <input type="number" className="form-control" maxLength="10"
                                                            min="0" value={ this.state.transpotationCost } onChange={e => this.setState({transpotationCost: e.target.value})}
                                                            placeholder="Transportation cost" />
                                                    </div>
                                                </div>

                                                <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Drop shipping minimum order&nbsp;</label>
                                                    <div className="col-sm-9 pr-35" style={{textAlign:'left'}}>
                                                        <input type="checkbox"
                                                            name="checkboxMinOrderAmount" onChange= {e => {this.setState({dropShipMinimumOrder: e.target.checked})}}
                                                            id="checkboxMinOrderAmount" checked={this.state.dropShipMinimumOrder}/>
                                                    </div>
                                                </div>

                                                { this.state.dropShipMinimumOrder ? <div className="form-group row">
                                                    <label htmlFor="inputPassword"
                                                        className="col-sm-3 col-form-label fontbold">Transportation cost without minimum&nbsp;</label>
                                                    <div className="col-sm-9 pr-35">

                                                        <input type="number" className="form-control"
                                                            min="0" maxLength="10" value= { this.state.transpotationCostWithoutMin }
                                                            onChange={ e=> {this.setState({transpotationCostWithoutMin: e.target.value})} } placeholder="Transportation cost without minimum" />
                                                    </div>
                                                </div> : null }</div> : null}

                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                </div>
                                </div>
                            </form>
                        </div>
                    </ModalBody >
                </Modal >
            </div>
        );
    }
}
export default withTranslation ()(EditCustomerModal); 