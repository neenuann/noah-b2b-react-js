import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Redirect
  } from "react-router-dom";
import Login  from './pages/login/login';
import Home from './pages/home/home';
const createRoutes = () => (
    <Router>
      <Route exact path="/">
        {localStorage.getItem("token") ? <Redirect to="/home" /> : <Redirect to="/login" />}
      </Route>
      <Route exact path="/login" component= {Login}/>
      <Route exact path="/home" component={Home}/>
    </Router>
);

export default createRoutes;