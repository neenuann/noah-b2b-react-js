import React from "react";
import ReactDOM from 'react-dom';
import createRoutes from './router';
const routes = createRoutes();

ReactDOM.render(
  routes,
  document.getElementById('root')
);
