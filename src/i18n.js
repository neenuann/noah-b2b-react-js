import i18next from 'i18next';
import { initReactI18next } from "react-i18next";
import translationEN from './locales/en/translation.json';
import translationDU from './locales/du/translation.json';
import translationFR from './locales/fr/translation.json';

const resources = {
  en: {
    translation: translationEN
  },
  du: {
    translation: translationDU
  },
  fr: {
    translation: translationFR
  }
};

i18next
.use(initReactI18next)
  .init({
    interpolation: {
      // React already does escaping
      escapeValue: false,
    },
    fallbackLng: "en",
    resources,
  })

export default i18next