import React from 'react';
import { Redirect } from "react-router-dom";
import DashboardLayout from '../dashboard-layout/dashboard-layout'
import SupplierDashboard from '../supplier-dashboard/supplier-dashboard'

function Home() {
  return (
    <div>
    { !localStorage.getItem("token") ?  <Redirect to="/login" /> : 
    <div>
      <DashboardLayout/>
      <SupplierDashboard/>
    </div>
  }
  </div>
  );
}
 
export default Home;