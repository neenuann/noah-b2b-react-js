import React, { useState } from "react";
import './supplier-dashboard.css'
import Customer from './customer/customer'
import Test from './product-brands/productbrands'
export default function SupplierDashboard(props) {
    
    const [activeTab, setActiveTab] = useState("Customers");
    function changeTab(data) {
        setActiveTab(data);
    }
    return (
        <div style={{borderBottom: '1px solid #cfdbe2', padding: '10px 15px 39px 15px;background: #fafbfc'}}>
            <div className="container-fluid">
                <div className="row" style={{minHeight: '43px'}}>
                    <div className="col-6">
                        <h5 className="pt-1 mb-1" style={{marginLeft: '-9px !important'}}>
                        { activeTab }</h5>
                    </div>
                </div>
            </div>

            <div className="container-fluid mt-3 c-wrapper">
                <div className="row">

                    <div className="col-12">
                        <div className="rec-shap">
                            <div className="tab-content" id="nav-tabContent">
                                <nav>
                                    <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a className="nav-item nav-link navborder active" id="nav-1-tab" data-toggle="tab" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true" onClick={() => changeTab('Customers')}>
                                            Customers
                                </a>

                                        <a className="nav-item nav-link navborder" id="nav-4-tab" data-toggle="tab" href="#nav-4" role="tab" aria-controls="nav-4" aria-selected="false" onClick={() => changeTab('ProductBrand')}>
                                            ProductBrand
                                </a>
                                    </div>
                                </nav>
                                <div className="tab-content" id="nav-tabContent">
                                    {activeTab === 'Customers' ? <div className="tab-pane fade show active" id="nav-1" role="tabpanel" aria-labelledby="nav-1-tab">
                                        <br />
                                        <Customer/>
                                    </div> : activeTab === 'ProductBrand' ?

                                    <div className="tab-pane fade show active" id="nav-4" role="tabpanel" aria-labelledby="nav-4-tab">
                                        <br />
                                        <Test/>
                            </div>: null}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}