import React, {Component} from "react";
import  './productbrand.css';
import DataTable from 'react-data-table-component';
import axios from 'axios';
import Modal from '../../../components/modal/modal';
import fetchProducts from "../../../services/api";

class ProductBrands extends Component {
   constructor() {
     super();
     this.state = {
       data: null,
       isShowing: false
     };
   }
    filePath = '';
    baseUrl = 'http://localhost:8080';

    componentDidMount() {
     this.loadData();
    }

    previewImage = (event) => {
     this.filePath = event.currentTarget.src;
     this.setState({
       isShowing: true
     });
    }

    closeModalHandler = () => {
      this.setState({
          isShowing: false
      });
  }

  renderImage(row) {
    let src;
    const id = localStorage.getItem('id');
    if (row.filePath) {
      src = this.baseUrl + '/supplier/v1/supplier/' + id + '/brand/image?file=' + row.filePath;
    } else {
      src = '/assets/images/nav-logo.png'
    }
    return <img src={src} onClick={e => this.previewImage(e)} className="img-fluid img-thumbnail" />
  }

  loadData() {
    const supplierId = localStorage.getItem('id');
    const headers = {
     'Content-Type': 'application/json'
    }
    axios.get(this.baseUrl + '/supplier/v1/supplier/' + supplierId + '/brand', { headers: headers, withCredentials: true}).then(response => {
      const data = response.data;
      this.setState({data: data['product_brands']});
      //this.itemList = data['product_brands'];
    }).catch(err=> {
       console.log('error');
    });
  }

   uploadImage(fileInput, brandId) {
     if (fileInput.target.files.length > 0) {
       const file = fileInput.target.files[0];
       const formData = new FormData();
       formData.append('file', file);
       var supplierId = localStorage.getItem('id');
       const endpoint = this.baseUrl + '/supplier/v1/supplier/' + supplierId + '/brand/' + brandId + '/image';
       return axios.post(endpoint, formData, {withCredentials: true}).then(response =>{
        this.loadData();
       }).catch(err => {

       });
     }
   }

  getColumns() {
      return [
        {
            name: 'Brand Name',
            selector: 'name',
            sortable: true,
            wrap: true
          },
          {
            name: 'Image Link',
            selector: 'moodImageUrl',
            sortable: true,
            wrap: true,
          },
          {
            name: 'Image',
            selector: 'filePath',
            sortable: true,
            wrap: true,
            cell: row => this.renderImage(row)
          },
          {
            cell: row => <div className="min-padding upload-btn-wrapper"><input type="file" id="file" accept="image/*" name="myfile" onChange={e => this.uploadImage(e, row.id)}/><button type="button" className="btn btn-primary btn-action bluebg">Upload Image</button></div>,
          }
        ];
    }

    render() {
      if(this.state.data !== null) {
        return  <div className="tableResponsive">
          <DataTable data={this.state.data} columns={this.getColumns()} className="row-border hover" style={{ width: '100%'}} />
          <Modal show={this.state.isShowing} close={this.closeModalHandler}><img src={this.filePath} className="img-fluid img-thumbnail" /></Modal>
        </div>
      } else {
        return <div>Loading....</div>
      }
    }
}

export default ProductBrands;