import React, { useEffect, useState } from "react"
import DataTable from 'react-data-table-component';
import axios from 'axios';
import EditCustomerModal from '../../../components/edit-customer-modal/edit-customer-modal';
import fetchCustomers from "../../../services/api";

function Customer() {
    const [data, setData] = useState(null)
    const [isEditCustomer, setIsEditCustomer] = useState(false)
    const [ customerId, setCustomerId ] = useState(0)

     useEffect(() => {
        loadCustomers();
      }, [])

      const loadCustomers = () => {
        fetchCustomers()
        .then( (res) => {
          const itemArr = res['data'];
          setData(itemArr['data']);
        }).catch(error=>{
          console.log(error);
       });
      }
      
      const openCustomerView = rowId => {
        setIsEditCustomer(true);
        setCustomerId(rowId);
      }

      const formatRow = (data) => {
        if(data != null) {
          if(Number(data)===0){
            return 'Not responded';
          }
          else if(Number(data)===1){
            return 'Accepted';
          } else if(Number(data)===2){
            return 'Rejected';
          }
          else {
            return '';
          }
        }
        else {
          return null;
        }
      }

      const closeHandler = () => {
        setIsEditCustomer(false);
      }

      const columns =
        [
            {
                name: 'Customers',
                selector: 'name',
                sortable: true,
                cell: row => <a style={{textDecoration: "none", color: "#5d9cec", cursor: "pointer"}}>{row.name}</a>,
                wrap: true,
              },
              {
                name: 'Customer Group',
                selector: 'categoryName',
                sortable: true,
                wrap: true,
              },
              {
                name: 'Contact Person',
                selector: 'contactPerson',
                sortable: true,
                wrap: true,
              },
              {
                name: 'VAT-NUMBER',
                selector: 'vat',
                sortable: true,
                wrap: true,
              },
              {
                name: '@',
                selector: 'email',
                sortable: true,
                
              },
              {
                name: 'Country',
                selector: 'country',
                sortable: true,
                wrap: true,
              },
             {
                name: 'Status',
                selector: 'notificationStatus',
                cell: row => formatRow(row.notificationStatus),
                wrap: true,
             },
             {
                cell: row => <div className="hidden-sm hidden-xs text-right"><div id="ember1114"  
                className="btn btn-sm btn-default table-link ember-view"><em className="fa fa-search"></em></div><div id="ember1114" onClick={() => openCustomerView(row.id)} 
                className="btn btn-sm btn-default table-link ember-view"><em className="fa fa-pencil"></em></div></div>,
                wrap: true,
             }]

      return (
          data !== null ?
          (isEditCustomer ? <div><DataTable data={ data } columns={columns } />
          <EditCustomerModal showModal={isEditCustomer} data= { customerId } onLoad={loadCustomers} close={ closeHandler}/></div>: 
          <DataTable data={data} columns={columns } />) : <div>Loading...</div>
      )

}
export default  Customer