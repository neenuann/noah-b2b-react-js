import React, { useState } from "react";
import {Redirect} from 'react-router-dom';
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import axios from 'axios';
import "./login.css";
import Cookies from 'universal-cookie';
import { useTranslation } from 'react-i18next';

export default function Login(props) 
{
  const id = 1;
  const { t, i18n } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loggedIn, setLoggedIn] = useState(false);
  const [language, setLanguage] = useState("en");
  function handleLogin() {
    var login = { "email": email, "password": password };
    setLoading(true);
    axios.post('http://localhost:8080/auth/v1/session', login).then(res => {
      const data = res['data'];
      localStorage.setItem('id', data['id']);
      localStorage.setItem('token', data['token']);
      const cookies = new Cookies();
      cookies.set('token',  data['token'], {path: "/", maxAge: 604800});
      cookies.set('uuid', data['uuid'], {path: "/", maxAge: 604800});
      setLoading(false);
      setLoggedIn(true);
      console.log('success');
      //props.history.push('/home');

    }).catch(error => {
      setLoading(false);
      console.log('error');
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

  const changeLanguage = (lng) => {
    setLanguage(lng);
    i18n.changeLanguage(lng);
  }

  return (
    localStorage.getItem("token") ? <Redirect to='home'/>:
    <form onSubmit={handleSubmit}>
        <select className="mdb-select md-form language" onChange={e =>  changeLanguage(e.target.value)}>
           <option value="en">EN</option>
           <option value="du">NL</option>
           <option value="fr">FR</option>
        </select><br />
     <div className="login-signup center-login">
        <div className="login-bg-img"> </div>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="rec-shap">
                            <div className="loginContainer">
                                <div className="text-center mt-4 mb-4"><img src="assets/images/logo.png" className="img-fluid" alt=""/></div>
                                
                                <FormGroup controlId="email" className="form-group">
                                    <FormLabel>{t('login.Emailaddress')}</FormLabel>
                                      <FormControl className="form-control"
                                          autoFocus
                                          type="email"
                                          value={email}
                                          onChange={e => setEmail(e.target.value)}
                                      />
                                </FormGroup>
                                
                                <FormGroup controlId="password" className="form-group">
                                    <FormLabel>{t('login.Password')}</FormLabel>
                                    <FormControl type="password" className="form-control"  value={password}
                                    onChange={e => setPassword(e.target.value)}/>
                                </FormGroup>
                                
                                <div className="form-group form-check">
                                    
                                   
                                <div className="form-group text-right">
                                    <Button block onClick= {handleLogin} disabled={loading} type="submit" className="btn login-button">Login</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    

  );
}