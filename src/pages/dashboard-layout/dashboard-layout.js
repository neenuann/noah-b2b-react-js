import React, { useState } from "react";
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem } from 'mdbreact';
import "./dashboard-layout.css"
import Cookies from "universal-cookie";
import { Redirect } from "react-router-dom";

export default function DashboardLayout() 
{
    const [loggedOut, setLoggedOut] = useState(false);
    const handleLogOut = () => {
        new Cookies().remove('token');
        new Cookies().remove('uuid');
        localStorage.clear();
        setLoggedOut(true);       
    }

    const navBg = {height: '55px', backgroundColor: '#5d9cec'}
    return(
        loggedOut ? <Redirect to='/login'/>:
        <MDBNavbar style={navBg} dark expand="lg" className="navCss">
        <MDBNavbarBrand>
        <a className="navbar-brand">
        <img src="assets/images/nav-logo.png" height="40px;" width="40px"/>
      </a>
        </MDBNavbarBrand>
          <MDBNavbarNav left className="navbar-nav mr-auto">
            <MDBNavItem className="nav-item">
            <a className="nav-link waves-light"><i className="fa fa-user" aria-hidden="true"></i></a>
            </MDBNavItem>
            <MDBNavItem>
            <a className="nav-link waves-light" onClick={handleLogOut}><i className="fa fa-lock" aria-hidden="true"></i></a>
            </MDBNavItem>
          </MDBNavbarNav>
      </MDBNavbar>
);
}
